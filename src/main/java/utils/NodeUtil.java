package utils;

import drone.Position;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

public class NodeUtil
{
    public static Position getNodePosition(Node node)
    {
        return new Position(node.<Integer>getAttribute("x"), node.<Integer>getAttribute("y"));
    }

    public static int getNodeX(Node node)
    {
        return node.<Integer>getAttribute("x");
    }

    public static int getNodeY(Node node)
    {
        return node.<Integer>getAttribute("y");
    }

    public static int getDensity(Node n)
    {
        String nodeAtt = n.getAttribute("ui.style");
        if (nodeAtt != null)
        {
            return Integer.parseInt(nodeAtt.split(",")[1]);
        }
        return 255;
    }

    public static int getOpacity(Node n)
    {
        String nodeAtt = n.getAttribute("ui.style");
        if (nodeAtt != null)
        {
            String afterSplit = nodeAtt.split(",")[3];
            return Integer.parseInt(afterSplit.substring(0, afterSplit.length() - 2));
        }
        return 0;
    }

    public static Node getNodeByPosition(Graph graph, Position position)
    {
        return graph.getNodeSet().stream().filter((Node node) -> getNodePosition(node).equals(position)).filter(node -> !node.getId().contains("drone")).findFirst().orElse(null);
    }

    public static Node[] getNodesWithinRange(Graph graph, int xFrom, int xTo, int yFrom, int yTo)
    {
        return graph.getNodeSet().stream().filter((Node node) -> getNodePosition(node).isWithinRange(xFrom, xTo, yFrom, yTo)).filter(node -> !node.getId().contains("drone")).toArray(Node[]::new);
    }

    public static synchronized void synchronizedSetAttribute(Node node, String attribute, Object value)
    {
        node.setAttribute(attribute, value);
    }

    public static double calculateDistanceBetween(Node first, Node second)
    {
        Position one = getNodePosition(first);
        Position two = getNodePosition(second);
        return Math.sqrt(Math.pow(one.getX() - two.getX(), 2.0) + Math.pow(one.getY() - two.getY(), 2.0));
    }

}
