package utils;

import cloud.Direction;
import cloud.PollutionCloud;
import drone.Drone;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import java.util.*;
import java.util.stream.Collectors;

public class Runner
{
    private static final Random random = new Random();

    private static final Direction[] upperLeft = {Direction.LEFT, Direction.UP, Direction.LEFT_UP};
    private static final Direction[] upperRight = {Direction.RIGHT, Direction.UP, Direction.RIGHT_UP};
    private static final Direction[] lowerLeft = {Direction.LEFT, Direction.DOWN, Direction.LEFT_DOWN};
    private static final Direction[] lowerRight = {Direction.RIGHT, Direction.DOWN, Direction.RIGHT_DOWN};

    private static void step(Graph graph, boolean changeDensityAndOpacity, Direction direction, int graphX)
    {
        Collection<Node> collection = graph.getNodeSet().stream().filter(node -> !node.getId().contains("drone")).collect(Collectors.toList());
        if (changeDensityAndOpacity)
        {
            PollutionCloud.changeNodesDensityAndOpacity(collection);
        }
        moveCloud(graph, direction, graphX);
    }

    private static void moveCloud(Graph graph, Direction direction, int graphX)
    {
        //System.out.println("Moving " + direction);
        Collection<Node> collection = graph.getNodeSet().stream().filter(node -> !node.getId().contains("drone")).collect(Collectors.toList());
        Map<Integer, List<Node>> rowMap = collection.stream().collect(Collectors.groupingBy((Node node) -> node.<Integer>getAttribute("y")));
        Map<Integer, List<Node>> columnMap = collection.stream().collect(Collectors.groupingBy((Node node) -> node.<Integer>getAttribute("x")));
        Direction first = null;
        Direction second = null;
        switch (direction)
        {
            case LEFT:
            case RIGHT:
                for (Integer i = 0; i < graphX; i++)
                {
                    PollutionCloud.moveRowOrColumn(rowMap.get(i).stream().sorted(Comparator.comparingInt(node -> node.<Integer>getAttribute("x"))).toArray(Node[]::new), direction);
                }
                break;
            case UP:
            case DOWN:
                for (Integer i = 0; i < graphX; i++)
                {
                    PollutionCloud.moveRowOrColumn(columnMap.get(i).stream().sorted(Comparator.comparingInt(node -> node.<Integer>getAttribute("y"))).toArray(Node[]::new), direction);
                }
                break;
            default:
                if (direction == Direction.RIGHT_DOWN)
                {
                    first = Direction.RIGHT;
                    second = Direction.DOWN;
                } else if (direction == Direction.LEFT_DOWN)
                {
                    first = Direction.RIGHT;
                    second = Direction.DOWN;
                } else if (direction == Direction.LEFT_UP)
                {
                    first = Direction.LEFT;
                    second = Direction.UP;
                } else if (direction == Direction.RIGHT_UP)
                {
                    first = Direction.RIGHT;
                    second = Direction.UP;
                } else
                {
                    break;
                }
                for (Integer i = 0; i < graphX; i++)
                {
                    PollutionCloud.moveRowOrColumn(rowMap.get(i).stream().sorted(Comparator.comparingInt(node -> node.<Integer>getAttribute("x"))).toArray(Node[]::new), first);
                }
                for (Integer i = 0; i < graphX; i++)
                {
                    PollutionCloud.moveRowOrColumn(columnMap.get(i).stream().sorted(Comparator.comparingInt(node -> node.<Integer>getAttribute("y"))).toArray(Node[]::new), second);
                }
                break;
        }

    }

    private static Direction[] chooseDirectionSet()
    {
        switch (random.nextInt(4))
        {
            case 0:
                return upperLeft;
            case 1:
                return upperRight;
            case 2:
                return lowerLeft;
            default:
            case 3:
                return lowerRight;
        }
    }

    private static Direction chooseDirection(Direction[] directions)
    {
        switch (random.nextInt(8))
        {
            case 0:
                return directions[0];
            case 1:
                return directions[1];
            case 2:
                return directions[2];
            default:
                return Direction.NONE;
        }
    }

    public static void iterate(Graph graph, int graphX, int n, long timeMillisWait)
    {
        Direction[] directions = chooseDirectionSet();

        boolean changeDensity = false;
        for (int i = 0; i < n; i++)
        {
            try
            {
                Thread.sleep(timeMillisWait);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            if (i % 5 == 0)
            {
                changeDensity = true;
            }
            step(graph, changeDensity, chooseDirection(directions), graphX);
            changeDensity = false;
        }

    }

    public static void runDroneThreadpool(Graph graph, int threads, int xLimit)
    {
        Random random = new Random();

        List<Drone> drones = new ArrayList<>();
        for (int i = 0; i < threads; i++)
        {
            drones.add(new Drone(random.nextInt(xLimit), random.nextInt(xLimit)));
            drones.get(i).addToGraph(graph, xLimit, drones.get(i).getxPosition(), drones.get(i).getyPosition());
        }

        for (int i = 0; i < drones.size(); i++)
        {
            for (int j = 0; j < drones.size(); j++)
            {
                if (i != j)
                {
                    drones.get(i).addLinkBetween(drones.get(j));
                }
            }
        }

        for (Drone drone : drones)
        {
            new Thread(new Drone.Mover(drone)).start();
        }

    }

}
