package cloud;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import utils.NodeUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static utils.NodeUtil.synchronizedSetAttribute;

public class PollutionCloud
{
    private static final double GEN_CLOUD_PROB_THRESHOLD = 0.1;
    private static final int DENSITY_THRESHOLD = 200;
    private static final int OPACITY_LOWER_THRESHOLD = 55;
    private static final int OPACITY_HIGHER_THRESHOLD = 100;

    private static final String STYLE_BLACK_NON_TRANSPARENT = "fill-color: rgba(0,0,0,255);";
    private static final String STYLE_WHITE_TRANSPARENT = "fill-color: rgba(255,255,255,0);";

    public static void applyCloud(Graph graph, int cloudSize, int graphX)
    {
        graph.addAttribute("ui.stylesheet", "graph { fill-color: rgb(60,60,135); } edge { fill-color: rgb(60,60,135); }");
        for (Node n : graph.getNodeSet())
        {
            n.addAttribute("ui.style", STYLE_WHITE_TRANSPARENT);
        }
        Node random = graph.getNodeSet().stream().filter(node -> node.<Integer>getAttribute("x") == graphX / 2 && node.<Integer>getAttribute("y") == graphX / 2).collect(Collectors.toList()).get(0);
        random.setAttribute("ui.style", STYLE_BLACK_NON_TRANSPARENT);
        Iterator<Node> iterator = random.getNeighborNodeIterator();
        iterator.forEachRemaining(n -> n.addAttribute("ui.style", STYLE_BLACK_NON_TRANSPARENT));
        for (int i = 0; i < cloudSize; i++)
        {
            for (Node n : graph.getNodeSet())
            {
                List<Node> neighbours = new ArrayList<>();
                n.getNeighborNodeIterator().forEachRemaining(neighbours::add);
                if (neighbours.stream().anyMatch(neighbour -> NodeUtil.getDensity(neighbour) == 0) && Math.random() < GEN_CLOUD_PROB_THRESHOLD)
                {
                    n.addAttribute("ui.style", STYLE_BLACK_NON_TRANSPARENT);
                }
            }
        }
    }

    private static int calculateColorDensity(Node node)
    {
        List<Integer> neighbourDensities = new ArrayList<>();
        node.getNeighborNodeIterator().forEachRemaining(n -> {
            String att = n.getAttribute("ui.style");
            if (att == null)
            {
                neighbourDensities.add(255);
            } else
            {
                neighbourDensities.add(Integer.parseInt(att.split(",")[1]));
            }
        });
        return (int) neighbourDensities.stream().mapToInt(i -> i).average().orElse(0);
    }

    private static int calculateOpacity(Node node)
    {
        List<Integer> neighbourTransparencyValues = new ArrayList<>();
        node.getNeighborNodeIterator().forEachRemaining(n -> {
            neighbourTransparencyValues.add(NodeUtil.getOpacity(n));
        });
        return (int) neighbourTransparencyValues.stream().mapToInt(i -> i).average().orElse(0);
    }

    private static synchronized void setDensity(Node n, int density, int opacity)
    {
        synchronizedSetAttribute(n, "ui.style", "fill-color: rgba(" + density + "," + density + "," + density + "," + opacity + ");");
    }

    public static synchronized void changeNodesDensityAndOpacity(Collection<Node> nodes)
    {
        for (Node node : nodes)
        {
            int density = calculateColorDensity(node);
            int opacity = calculateOpacity(node);
            if (density < DENSITY_THRESHOLD)
            {
                synchronizedSetAttribute(node, "ui.style", "fill-color: rgba(" + density + "," + density + "," + density + "," + opacity + ");");
            } else
            {
                synchronizedSetAttribute(node, "ui.style", STYLE_WHITE_TRANSPARENT);
            }
        }
    }

    public static void moveRowOrColumn(Node[] nodes, Direction direction)
    {
        switch (direction)
        {
            // Moving rows
            case RIGHT:
                for (int i = nodes.length - 2; i >= 0; i--)
                {
                    setDensity(nodes[i + 1], NodeUtil.getDensity(nodes[i]), NodeUtil.getOpacity(nodes[i]));
                }
                break;
            case LEFT:
                for (int i = 1; i < nodes.length; i++)
                {
                    setDensity(nodes[i - 1], NodeUtil.getDensity(nodes[i]), NodeUtil.getOpacity(nodes[i]));
                }
                break;
            // Moving columns
            case DOWN:
                for (int i = 1; i < nodes.length; i++)
                {
                    setDensity(nodes[i - 1], NodeUtil.getDensity(nodes[i]), NodeUtil.getOpacity(nodes[i]));
                }
                break;
            case UP:
                for (int i = nodes.length - 2; i >= 0; i--)
                {
                    setDensity(nodes[i + 1], NodeUtil.getDensity(nodes[i]), NodeUtil.getOpacity(nodes[i]));
                }
                break;
        }
    }

    public static boolean isCloudWithinRange(Node node)
    {
        int opacity = NodeUtil.getOpacity(node);
        return opacity > OPACITY_LOWER_THRESHOLD && opacity < OPACITY_HIGHER_THRESHOLD;
    }

}
