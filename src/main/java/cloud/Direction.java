package cloud;

public enum Direction
{
    NONE(0), LEFT(1), UP(2), RIGHT(3), DOWN(4), LEFT_UP(5), RIGHT_UP(6), LEFT_DOWN(7), RIGHT_DOWN(8);

    private final int value;

    Direction(int value) { this.value = value; }

    public int getValue() { return value; }

}
