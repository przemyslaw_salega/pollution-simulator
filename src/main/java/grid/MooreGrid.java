package grid;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;

import java.util.Random;

public class MooreGrid
{

    public static void main(String[] args)
    {
        int limit = new Random().nextInt(12);
        Graph graph = moore(limit);
        graph.display();
    }

    private static void addEdges(Graph graph, int limit, Node[][] nodes)
    {
        for (int i = 0; i < limit; i++)
        {
            for (int j = 0; j < limit; j++)
            {
                if (j < limit - 1)
                {
                    graph.addEdge(nodes[i][j].getId() + nodes[i][j + 1].getId(), nodes[i][j], nodes[i][j + 1]);
                }
                if (i < limit - 1)
                {
                    graph.addEdge(nodes[i][j].getId() + nodes[i + 1][j].getId(), nodes[i][j], nodes[i + 1][j]);
                }
                if (j > 0 && i < limit - 1)
                {
                    graph.addEdge(nodes[i][j].getId() + nodes[i + 1][j - 1].getId(), nodes[i][j], nodes[i + 1][j - 1]);
                }
                if (j < limit - 1 && i < limit - 1)
                {
                    graph.addEdge(nodes[i][j].getId() + nodes[i + 1][j + 1].getId(), nodes[i][j], nodes[i + 1][j + 1]);
                }
            }
        }
    }

    public static Graph moore(int xLimit)
    {
        Graph graph = new SingleGraph("graph");
        Node[][] nodes = new Node[xLimit][xLimit];
        for (int i = 0; i < xLimit; i++)
        {
            for (int j = 0; j < xLimit; j++)
            {
                nodes[i][j] = graph.addNode(Integer.toString(i * xLimit + j));
                nodes[i][j].setAttribute("x", j);
                nodes[i][j].setAttribute("y", i);
            }
        }
        addEdges(graph, xLimit, nodes);
        return graph;
    }

}
