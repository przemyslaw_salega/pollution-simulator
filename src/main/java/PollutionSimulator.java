import cloud.PollutionCloud;
import grid.MooreGrid;
import org.graphstream.graph.Graph;
import utils.Runner;

public class PollutionSimulator
{
    private static final int NUMBER_OF_DRONES = 10;

    public static void main(String[] args)
    {
        Graph graph = MooreGrid.moore(SimulationConstants.X_LIMIT);

        PollutionCloud.applyCloud(graph, 50, SimulationConstants.X_LIMIT);

        graph.display(false);

        Runner.runDroneThreadpool(graph, NUMBER_OF_DRONES, SimulationConstants.X_LIMIT);

        Runner.iterate(graph, SimulationConstants.X_LIMIT, 10000, SimulationConstants.WAIT_MILLIS);
    }
}
