package drone;

import cloud.Direction;
import cloud.PollutionCloud;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import utils.NodeUtil;

import java.util.*;


public class Drone
{

    // Drone sees FIELD_OF_VIEW values in front, on the left, right, behind it and so on.
    private static final int FIELD_OF_VIEW = 5;
    private static final int DISTANCE_BETWEEN_DRONES = 10;
    private static final long MILLIS_WAIT = 500;

    private static int counter = 0;
    private final String id;
    private Position position;
    private Node node;
    private Graph graph;
    private final Random random = new Random();

    private boolean shouldStop;

    private final Set<Position> previousPositions;

    public static class Mover implements Runnable
    {
        Drone drone;

        public Mover(Drone drone)
        {
            this.drone = drone;
        }

        @Override
        public void run()
        {
            while (true)
            {
                Optional<Position> optionalPosition = drone.analyzeAir();
                if (optionalPosition.isPresent())
                {
                    if (!PollutionCloud.isCloudWithinRange(NodeUtil.getNodeByPosition(drone.graph, drone.position)))
                    {
                        Drone.goToPosition(drone, optionalPosition.get());
                    }
                } else
                {
                    Optional<Position> avgPosition = drone.askNeighboursForPosition();
                    if (avgPosition.isPresent())
                    {
                        //System.out.println(drone + ": Moving to pos " + avgPosition.get());
                        Drone.goToPosition(drone, avgPosition.get());
                    } else
                    {
                        //System.out.println("Moving randomly");
                        drone.moveDroneRandomly();
                    }
                }
                Iterator<Node> iterator = drone.node.getNeighborNodeIterator();
                while (iterator.hasNext())
                {
                    Node neighbour = iterator.next();
                    if (drone.isTooCloseTo(neighbour))
                    {
                        drone.setNotOptimalPositionFlag(true);
                        drone.moveDroneWithinCloudRangeFrom(neighbour);
                        drone.setNotOptimalPositionFlag(false);
                    }
                }
            }
        }
    }

    public Drone(Position position)
    {
        shouldStop = false;
        this.id = "drone" + counter++;
        this.position = position;
        previousPositions = new HashSet<>();
    }

    public Drone(int x, int y)
    {
        this(new Position(x, y));
    }

    public void addToGraph(Graph graph, int xLimit, int x, int y)
    {
        this.graph = graph;
        node = graph.addNode(id);
        node.setAttribute("stop", false);
        if (x >= xLimit || y >= xLimit)
        {
            node.setAttribute("x", 0);
            node.setAttribute("y", 0);
        } else
        {
            node.setAttribute("x", x);
            node.setAttribute("y", y);
        }
        node.setAttribute("ui.style", "fill-color: red; size: 20px;");
    }

    public void addLinkBetween(Drone drone)
    {
        if (!node.hasEdgeBetween(drone.node))
        {
            this.graph.addEdge(this.node.getId() + drone.node.getId(), this.node, drone.node);
        }
    }

    public String getId()
    {
        return id;
    }

    public int getxPosition()
    {
        return position.getX();
    }

    public boolean setPosition(int x, int y)
    {
        if (!shouldStop || isNotOptimalPosition())
        {
            previousPositions.add(position);
            this.position = new Position(x, y);
            synchronized (this)
            {
                NodeUtil.synchronizedSetAttribute(node, "x", position.getX());
                NodeUtil.synchronizedSetAttribute(node, "y", position.getY());
            }
        }

        shouldStop = PollutionCloud.isCloudWithinRange(NodeUtil.getNodeByPosition(graph, position));
        if (shouldStop)
        {
            NodeUtil.synchronizedSetAttribute(node, "stop", true);
            //System.out.println(id + ": I should stop now");
            return false;
        }
        NodeUtil.synchronizedSetAttribute(node, "stop", false);
        return true;
    }

    public static void goToPosition(Drone drone, Position position)
    {
        int xDiff = position.getX() - drone.position.getX();
        int yDiff = position.getY() - drone.position.getY();
        Direction direction;
        while (xDiff != 0 || yDiff != 0)
        {
            if (xDiff < 0 && yDiff < 0)
            {
                direction = Direction.LEFT_DOWN;
            } else if (xDiff < 0 && yDiff > 0)
            {
                direction = Direction.LEFT_UP;
            } else if (xDiff > 0 && yDiff < 0)
            {
                direction = Direction.RIGHT_DOWN;
            } else if (xDiff > 0 && yDiff > 0)
            {
                direction = Direction.RIGHT_UP;
            } else if (xDiff < 0)
            {
                direction = Direction.LEFT;
            } else if (xDiff > 0)
            {
                direction = Direction.RIGHT;
            } else if (yDiff < 0)
            {
                direction = Direction.DOWN;
            } else
            {
                direction = Direction.UP;
            }

            if (!drone.moveDrone(direction) || drone.analyzeAir().isPresent())
            {
                break;
            }
            xDiff = position.getX() - drone.position.getX();
            yDiff = position.getY() - drone.position.getY();
            //System.out.println(drone.id + ": Moving " + direction + " to " + position + " from " + drone.position + " (" + xDiff + "," + yDiff + ")");
            try
            {
                Thread.sleep(MILLIS_WAIT);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }

    public boolean moveDrone(Direction direction)
    {
        int newX = getxPosition();
        int newY = getyPosition();
        switch (direction)
        {
            case LEFT:
                newX = getxPosition() - 1;
                newY = getyPosition();
                break;
            case RIGHT:
                newX = getxPosition() + 1;
                newY = getyPosition();
                break;
            case UP:
                newX = getxPosition();
                newY = getyPosition() + 1;
                break;
            case DOWN:
                newX = getxPosition();
                newY = getyPosition() - 1;
                break;
            case RIGHT_UP:
                newX = getxPosition() + 1;
                newY = getyPosition() + 1;
                break;
            case LEFT_UP:
                newX = getxPosition() - 1;
                newY = getyPosition() + 1;
                break;
            case RIGHT_DOWN:
                newX = getxPosition() + 1;
                newY = getyPosition() - 1;
                break;
            case LEFT_DOWN:
                newX = getxPosition() - 1;
                newY = getyPosition() - 1;
                break;
        }
        return setPosition(newX, newY);
    }

    private void moveDroneRandomly()
    {
        goToPosition(this, new Position(random.nextInt(100), random.nextInt(100)));
    }

    public Position calculateOptimalPositionTo(Node node)
    {
        double theta = Math.atan2(NodeUtil.getNodeY(this.node) - NodeUtil.getNodeY(node), NodeUtil.getNodeX(this.node) - NodeUtil.getNodeY(node));
        int x2 = (int) Math.ceil(NodeUtil.getNodeX(this.node) + Drone.DISTANCE_BETWEEN_DRONES * Math.cos(theta));
        int y2 = (int) Math.ceil(NodeUtil.getNodeY(this.node) + Drone.DISTANCE_BETWEEN_DRONES * Math.sin(theta));
        return new Position(x2, y2);
    }

    public void moveDroneWithinCloudRangeFrom(Node n)
    {
        Direction direction = Direction.NONE;
        if (isTooCloseTo(n))
        {
            Position pos = NodeUtil.getNodePosition(node);
            Position nodePosition = NodeUtil.getNodePosition(n);
            if (pos.getX() - nodePosition.getX() < 0)
            {
                direction = Direction.LEFT;
            } else if (pos.getX() - nodePosition.getX() > 0)
            {
                direction = Direction.RIGHT;
            } else if (pos.getY() - nodePosition.getY() < 0)
            {
                direction = Direction.DOWN;
            } else if (pos.getY() - nodePosition.getY() > 0)
            {
                direction = Direction.UP;
            } else if (pos.getX() - nodePosition.getX() < 0 && pos.getY() - nodePosition.getY() > 0)
            {
                direction = Direction.LEFT_UP;
            } else if (pos.getX() - nodePosition.getX() < 0 && pos.getY() - nodePosition.getY() < 0)
            {
                direction = Direction.LEFT_DOWN;
            } else if (pos.getX() - nodePosition.getX() > 0 && pos.getY() - nodePosition.getY() > 0)
            {
                direction = Direction.RIGHT_UP;
            } else
            {
                direction = Direction.RIGHT_DOWN;
            }
        }
        //System.out.println(id + " moving " + direction + " from " + n.getId());
        goToPosition(this, analyzeAir(direction).orElse(new Position(NodeUtil.getNodeX(node), NodeUtil.getNodeY(node))));
    }

    public Optional<Position> analyzeAir()
    {
        return analyzeAir(Direction.NONE);
    }

    public Optional<Position> analyzeAir(Direction direction)
    {
        List<Position> positions = new ArrayList<>();
        Node[] nodes = NodeUtil.getNodesWithinRange(graph, getxPosition() - FIELD_OF_VIEW, getxPosition() + FIELD_OF_VIEW, getyPosition() - FIELD_OF_VIEW, getyPosition() + FIELD_OF_VIEW);
        for (Node node : nodes)
        {
            if (PollutionCloud.isCloudWithinRange(node))
            {
                positions.add(NodeUtil.getNodePosition(node));
            }
        }
        if (!positions.isEmpty())
        {
            if (direction == Direction.NONE)
            {
                int avgX = (int) positions.stream().mapToInt(Position::getX).average().orElse(0);
                int avgY = (int) positions.stream().mapToInt(Position::getY).average().orElse(0);
                return Optional.of(new Position(avgX, avgY));
            } else if (direction == Direction.UP)
            {
                return positions.stream().max(Comparator.comparingInt(Position::getY));
            } else if (direction == Direction.DOWN)
            {
                return positions.stream().min(Comparator.comparingInt(Position::getY));
            } else if (direction == Direction.LEFT)
            {
                return positions.stream().min(Comparator.comparingInt(Position::getX));
            } else if (direction == Direction.RIGHT)
            {
                return positions.stream().max(Comparator.comparingInt(Position::getX));
            }
        }
        return Optional.empty();
    }

    public synchronized Optional<Position> askNeighboursForPosition()
    {
        int avgX;
        int avgY;
        Iterator<Node> neighbours = this.node.getNeighborNodeIterator();
        List<Position> positions = new ArrayList<>();
        while (neighbours.hasNext())
        {
            Node neighbour = neighbours.next();
            Boolean stop = neighbour.<Boolean>getAttribute("stop");
            if (stop != null && stop)
            {
                positions.add(NodeUtil.getNodePosition(neighbour));
            }
        }
        avgX = (int) positions.stream().mapToInt(Position::getX).average().orElse(-1);
        avgY = (int) positions.stream().mapToInt(Position::getY).average().orElse(-1);

        if (avgX >= 0 && avgY >= 0)
        {
            return Optional.of(new Position(avgX, avgY));
        }
        return Optional.empty();
    }

    public boolean isTooCloseTo(Node node)
    {
        return NodeUtil.calculateDistanceBetween(this.node, node) < DISTANCE_BETWEEN_DRONES;
    }

    public int getyPosition()
    {
        return position.getY();
    }

    public Node getNode() { return node; }

    public void setNotOptimalPositionFlag(boolean value)
    {
        NodeUtil.synchronizedSetAttribute(node, "notOptimal", value);
    }

    public boolean isNotOptimalPosition()
    {
        Boolean value = node.<Boolean>getAttribute("notOptimal");
        return value != null ? value : false;
    }

}
