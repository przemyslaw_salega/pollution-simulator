package drone;

import java.util.Objects;

public class Position
{
    private final int x;
    private final int y;

    public Position(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    public boolean isWithinRange(int xFrom, int xTo, int yFrom, int yTo)
    {
        return x >= xFrom && x <= xTo && y >= yFrom && y <= yTo;
    }

    @Override
    public String toString()
    {
        return "[x = " + x + ", y = " + y + "]";
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return x == position.x && y == position.y;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(x, y);
    }
}
