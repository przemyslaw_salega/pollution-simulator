package drone;

import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class DroneTest
{
    Drone drone;
    Graph graph;

    @Before
    public void init()
    {
        drone = new Drone(15, 15);
        graph = new SingleGraph("graph");
        drone.addToGraph(graph, 100, drone.getxPosition(), drone.getyPosition());
    }

    @Test
    public void calculateOptimalPositionToTest()
    {
        Drone drone1 = new Drone(drone.getxPosition(), drone.getyPosition() + 1);
        drone1.addToGraph(graph, 100, drone1.getxPosition(), drone1.getyPosition());
        drone.addLinkBetween(drone1);
        assertEquals(new Position(drone1.getxPosition(), 26), drone1.calculateOptimalPositionTo(drone.getNode()));
    }
}